package com.itconsortiumgh.uniwallet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.uniwallet.model.db.PaymentsDto;
import com.itconsortiumgh.uniwallet.utils.GeneralUtils;
import com.itconsortiumgh.uniwallet.utils.SoapMessageReader;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SOAPCallbackProcessor {
	@Autowired
	GeneralUtils generalUtils;

	public PaymentsDto processCallback(String requestXml){
		String thirdPartyAcctRef="";
		String processingNumber="";
		String momTransactionId="";
		String statusCode="";
		String statusDesc="";
		
		try{
			thirdPartyAcctRef = SoapMessageReader.findSOAPField(requestXml, "ns3:ThirdPartyAcctRef");
			processingNumber = SoapMessageReader.findSOAPField(requestXml, "ns3:ProcessingNumber");
			momTransactionId = SoapMessageReader.findSOAPField(requestXml, "ns3:MOMTransactionID");
			statusCode = SoapMessageReader.findSOAPField(requestXml, "ns3:StatusCode").trim();
			statusDesc = SoapMessageReader.findSOAPField(requestXml, "ns3:StatusDesc");
			
			generalUtils.decodeAcctRef(thirdPartyAcctRef);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		PaymentsDto paymentsDto = new PaymentsDto();
		paymentsDto.setMomTransactionId(momTransactionId);
		paymentsDto.setProcessingNumber(processingNumber);
		paymentsDto.setStatusCode(statusCode);
		paymentsDto.setStatusDesc(statusDesc);
//		paymentsDto.setThirdPartyAcctRef(thirdPartyAcctRef);
		return paymentsDto;
	}
}

package com.itconsortiumgh.uniwallet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.uniwallet.model.CreditCustomerRequest;
import com.itconsortiumgh.uniwallet.model.DebitCustomerRequest;
import com.itconsortiumgh.uniwallet.model.db.ProductTable;
import com.itconsortiumgh.uniwallet.model.mtn.MTNRequest;
import com.itconsortiumgh.uniwallet.model.mtn.MTNResponse;
import com.itconsortiumgh.uniwallet.properties.ApplicationProperties;
import com.itconsortiumgh.uniwallet.utils.DBLogger;
import com.itconsortiumgh.uniwallet.utils.GeneralUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MTNMoMoService {
	@Autowired 
	RestTemplate restTemplate;
	@Autowired
	DBLogger dbLogger;
	@Autowired
	GeneralUtils generalUtils;
	@Autowired
	ApplicationProperties applicationProperties;
	
	public MTNResponse mtnCreditCustomer(CreditCustomerRequest creditCustomerRequest, MTNRequest mtnCreditCustomerRequest, ProductTable foundProductRecord){
		String url = applicationProperties.getMtnCreditCustomerUrl();

		mtnCreditCustomerRequest.setImsiNum(applicationProperties.getImsiNum());
		mtnCreditCustomerRequest.setSpId(foundProductRecord.getSpId());
		mtnCreditCustomerRequest.setSpPassword(foundProductRecord.getSpPassword());
		mtnCreditCustomerRequest.setServiceId(foundProductRecord.getServiceId());
		//encode acctRef
		String acctRef = generalUtils.encodeAccountRef(creditCustomerRequest.getMerchantId(), mtnCreditCustomerRequest.getAmount(), applicationProperties.getCharge(), creditCustomerRequest.getProductId(), creditCustomerRequest.getMsisdn(), foundProductRecord.getServiceId(), creditCustomerRequest.getNarration());
		mtnCreditCustomerRequest.setAcctRef(acctRef);
		
		mtnCreditCustomerRequest.setBalance(applicationProperties.getBalance());
		mtnCreditCustomerRequest.setMinDueAmount(applicationProperties.getMinDueAmount());
		mtnCreditCustomerRequest.setNarration(creditCustomerRequest.getNarration());
		log.info("************Hitting {} for a response***************",url);
		log.info("************the request going :{}",mtnCreditCustomerRequest);
		MTNResponse mtnCcResponse = restTemplate.postForObject(url, mtnCreditCustomerRequest, MTNResponse.class);
		log.info("***********the response coming :{}",mtnCcResponse);
		dbLogger.logTransactionsLogs(creditCustomerRequest, mtnCreditCustomerRequest, mtnCcResponse);
		return mtnCcResponse;
	}
	
	public MTNResponse mtnDebitCustomer(DebitCustomerRequest debitCustomerRequest, MTNRequest mtnDebitCustomerRequest,ProductTable foundProductRecord){
		String url = applicationProperties.getMtnDebitCustomerUrl();

		mtnDebitCustomerRequest.setImsiNum(applicationProperties.getImsiNum());
		mtnDebitCustomerRequest.setSpId(foundProductRecord.getSpId());
		mtnDebitCustomerRequest.setSpPassword(foundProductRecord.getSpPassword());
		mtnDebitCustomerRequest.setServiceId(foundProductRecord.getServiceId());
		mtnDebitCustomerRequest.setBalance(applicationProperties.getBalance());
		mtnDebitCustomerRequest.setMinDueAmount(applicationProperties.getMinDueAmount());
		mtnDebitCustomerRequest.setNarration(debitCustomerRequest.getNarration());
		//encode acctRef
		String acctRef = generalUtils.encodeAccountRef(debitCustomerRequest.getMerchantId(), mtnDebitCustomerRequest.getAmount(), applicationProperties.getCharge(), debitCustomerRequest.getProductId(), debitCustomerRequest.getMsisdn(), foundProductRecord.getServiceId(), debitCustomerRequest.getNarration());
		mtnDebitCustomerRequest.setAcctRef(acctRef);
		log.info("************Hitting {} for a response***************",url);
		log.info("************the request going :{}",mtnDebitCustomerRequest);
		MTNResponse mtnDcResponse = restTemplate.postForObject(url, mtnDebitCustomerRequest, MTNResponse.class);
		log.info("***********the response coming :{}",mtnDcResponse);
		dbLogger.logTransactionsLogs(debitCustomerRequest, mtnDebitCustomerRequest, mtnDcResponse);
		return mtnDcResponse;
	}

}

package com.itconsortiumgh.uniwallet.service;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.uniwallet.amqp.RabbitMQSubscriber;
import com.itconsortiumgh.uniwallet.model.AppConstants;
import com.itconsortiumgh.uniwallet.model.CreditCustomerRequest;
import com.itconsortiumgh.uniwallet.model.CreditCustomerResponse;
import com.itconsortiumgh.uniwallet.model.DebitCustomerRequest;
import com.itconsortiumgh.uniwallet.model.DebitCustomerResponse;
import com.itconsortiumgh.uniwallet.model.HttpResponseCode;
import com.itconsortiumgh.uniwallet.model.ResponseCode;
import com.itconsortiumgh.uniwallet.model.Status;
import com.itconsortiumgh.uniwallet.model.db.PaymentsDto;
import com.itconsortiumgh.uniwallet.model.db.ProductTable;
import com.itconsortiumgh.uniwallet.model.db.TransactionLogs;
import com.itconsortiumgh.uniwallet.model.mtn.MTNRequest;
import com.itconsortiumgh.uniwallet.model.mtn.MTNResponse;
import com.itconsortiumgh.uniwallet.model.smsgh.NetworkResponse;
import com.itconsortiumgh.uniwallet.model.smsgh.NumPortResponse;
import com.itconsortiumgh.uniwallet.model.tigo.TigoCreditCustomerRequest;
import com.itconsortiumgh.uniwallet.model.tigo.TigoCreditCustomerResponse;
import com.itconsortiumgh.uniwallet.model.tigo.TigoDebitCustomerRequest;
import com.itconsortiumgh.uniwallet.model.tigo.TigoDebitCustomerResponse;
import com.itconsortiumgh.uniwallet.model.vodafone.VodafoneCreditCustomerRequest;
import com.itconsortiumgh.uniwallet.model.vodafone.VodafoneCreditCustomerResponse;
import com.itconsortiumgh.uniwallet.model.vodafone.VodafoneDebitCustomerRequest;
import com.itconsortiumgh.uniwallet.model.vodafone.VodafoneDebitCustomerResponse;
import com.itconsortiumgh.uniwallet.properties.ApplicationProperties;
import com.itconsortiumgh.uniwallet.repository.NetworkRepository;
import com.itconsortiumgh.uniwallet.repository.PaymentRepository;
import com.itconsortiumgh.uniwallet.repository.TransactionLogRepository;
import com.itconsortiumgh.uniwallet.utils.DBLogger;
import com.itconsortiumgh.uniwallet.utils.GeneralUtils;
import com.itconsortiumgh.uniwallet.utils.JsonUtility;
import com.itconsortiumgh.uniwallet.utils.TransactionIdGenerator;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BusinessLogic {
	@Autowired
	MTNMoMoService mtnMoMoService;
	@Autowired
	VodafoneCashService vodafoneCashService;
	@Autowired
	TigoCashService tigoCashService;
	@Autowired 
	ApplicationProperties applicationProperties;
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	NetworkRepository networkRepository;
	@Autowired
	GeneralUtils generalUtils;
	@Autowired
	PaymentRepository paymentRepository;
	@Autowired
	RabbitMQSubscriber rabbitMQSubscriber;
	@Autowired
	TransactionLogRepository transactionLogRepository;
	@Autowired
	SOAPCallbackProcessor soapCallbackProcessor;
	@Autowired
	DBLogger dbLogger;
	
	public NumPortResponse getNetwork(String msisdn){
		NumPortResponse numPortResponse = new NumPortResponse();
		log.info("we are in the bnesslogic");
		String url = applicationProperties.getNumberPortabilityUrl()+msisdn;
		log.info("********the url: {}",url);
		String plainCreds = applicationProperties.getSmsghUsername()+":"+applicationProperties.getSmsghPassword();
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		
//
//		HttpHeaders headers = new HttpHeaders();
//		headers.add("Authorization", "Basic " + base64Creds);
//		
//		HttpEntity<String> request = new HttpEntity<String>(headers);
//		log.info("********the request: {}",request);
//		ResponseEntity<NetworkResponse> response = restTemplate.exchange(url, HttpMethod.GET, request, NetworkResponse.class);
//		NetworkResponse networkResponse = response.getBody();
//		log.info("********the response: {}",networkResponse);
//		
//		String network = networkResponse.getCurrentCarrier().getName().split(" ")[0];
////		String result = input.split("-")[0];
////		String result = input.substring(0, input.indexOf("-"));
//		numPortResponse.setNetwork(network);
		
		
		try {
			OkHttpClient client = new OkHttpClient();
			
			Request request = new Request.Builder()
					.url(url)
					.get()
//		  .addHeader("authorization", "Basic bGlzZGt6ZXA6Z2FteG1wY20=")
					.addHeader("authorization", "Basic "+base64Creds)
					.addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "7aae3c5a-7388-ebd1-be29-23df641ae15a")
					.build();
			
			log.info("************the request: {}",request);
			Response response = client.newCall(request).execute();
			log.info("************the response: {}",response);
			
			if(HttpResponseCode.OK == response.code()){
				String responseStr= response.body().string();
				log.info("************the response string: {}",responseStr);
				NetworkResponse networkResponse = JsonUtility.fromJson(responseStr, NetworkResponse.class);
				log.info("the networkResponse: {}",networkResponse);
				String network = networkResponse.getCurrentCarrier().getName().split(" ")[0];
				numPortResponse.setNetwork(network);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return numPortResponse;
	}
	
	public List<String> getNetworks(){
		return networkRepository.getNetworks();
	}
	
	public CreditCustomerResponse creditCustomer(CreditCustomerRequest creditCustomerRequest,ProductTable foundProductRecord){
		CreditCustomerResponse creditCustomerResponse = new CreditCustomerResponse();
		String network = creditCustomerRequest.getNetwork();
		
		if(AppConstants.MTN.equalsIgnoreCase(network)){
			MTNRequest mtnCreditCustomerRequest = new MTNRequest();
			BigDecimal amount = new BigDecimal(creditCustomerRequest.getAmount()).setScale(2, BigDecimal.ROUND_HALF_EVEN);
			String amountInPesewas = amount.toString().replace(".", "");
	        log.info("the amount in pesewas: {}",amountInPesewas);
			mtnCreditCustomerRequest.setAmount(amountInPesewas.toString());
			mtnCreditCustomerRequest.setMsisdn(creditCustomerRequest.getMsisdn());
			mtnCreditCustomerRequest.setProcessingNumber(TransactionIdGenerator.nextId().toString());
			MTNResponse mtnCreditCustomerResponse = mtnMoMoService.mtnCreditCustomer(creditCustomerRequest, mtnCreditCustomerRequest, foundProductRecord);
			log.info("***********the response :{}",mtnCreditCustomerResponse);
			if(AppConstants.MTN_SUCCESS.equalsIgnoreCase(mtnCreditCustomerResponse.getResponseCode())){
				creditCustomerResponse.setResponseCode(AppConstants.PENDING);
				creditCustomerResponse.setResponseMessage(AppConstants.PENDING_MESSAGE);
			}else{
				creditCustomerResponse.setResponseCode(mtnCreditCustomerResponse.getResponseCode());
				creditCustomerResponse.setResponseMessage(mtnCreditCustomerResponse.getResponseMessage());
			}
		}else if(AppConstants.TIGO.equalsIgnoreCase(network)){
			TigoCreditCustomerRequest tigoCreditCustomerRequest = new TigoCreditCustomerRequest();
			tigoCreditCustomerRequest.setAmount(creditCustomerRequest.getAmount());
			tigoCreditCustomerRequest.setApiKey(creditCustomerRequest.getApiKey());
			tigoCreditCustomerRequest.setMsisdn(creditCustomerRequest.getMsisdn());
			tigoCreditCustomerRequest.setRefNo(creditCustomerRequest.getRefNo());
			TigoCreditCustomerResponse tigoCreditCustomerResponse = tigoCashService.tigoCreditCustomer(tigoCreditCustomerRequest);
			log.info("***********the response :{}",tigoCreditCustomerResponse);
			if(AppConstants.PENDING.equalsIgnoreCase(tigoCreditCustomerResponse.getResponseCode())){
				creditCustomerResponse.setResponseCode(AppConstants.PENDING);
				creditCustomerResponse.setResponseMessage(AppConstants.PENDING_MESSAGE);
			}else{
				creditCustomerResponse.setResponseCode(tigoCreditCustomerResponse.getResponseCode());;
				creditCustomerResponse.setResponseMessage(tigoCreditCustomerResponse.getResponseMessage());
			}
			dbLogger.logTransactionLogs(creditCustomerRequest, creditCustomerResponse);
		}else if(AppConstants.VODAFONE.equalsIgnoreCase(network)){
			VodafoneCreditCustomerRequest vodafoneCreditCustomerRequest = new VodafoneCreditCustomerRequest();
			vodafoneCreditCustomerRequest.setRefNo(creditCustomerRequest.getRefNo());
			vodafoneCreditCustomerRequest.setApiKey(creditCustomerRequest.getApiKey());;
			vodafoneCreditCustomerRequest.setMsisdn(creditCustomerRequest.getMsisdn());
			vodafoneCreditCustomerRequest.setAmount(creditCustomerRequest.getAmount());
			VodafoneCreditCustomerResponse vodafoneCreditCustomerResponse = vodafoneCashService.vodafoneCreditCustomer(vodafoneCreditCustomerRequest);
			log.info("***********the response :{}",vodafoneCreditCustomerResponse);
			if(AppConstants.PENDING.equalsIgnoreCase(vodafoneCreditCustomerResponse.getResponseCode())){
				creditCustomerResponse.setResponseCode(AppConstants.PENDING);
				creditCustomerResponse.setResponseMessage(AppConstants.PENDING_MESSAGE);
			}else{
				creditCustomerResponse.setResponseCode(vodafoneCreditCustomerResponse.getResponseCode());
				creditCustomerResponse.setResponseMessage(vodafoneCreditCustomerResponse.getResponseMessage());
			}
			dbLogger.logTransactionLogs(creditCustomerRequest, creditCustomerResponse);
		}
//		dbLogger.logTransactionLogs(creditCustomerRequest, creditCustomerResponse);
		return creditCustomerResponse;
	}
	
	public DebitCustomerResponse debitCustomer(DebitCustomerRequest debitCustomerRequest,ProductTable foundProductRecord){
		DebitCustomerResponse debitCustomerResponse = new DebitCustomerResponse();
		String network = debitCustomerRequest.getNetwork();
		
		if(AppConstants.MTN.equalsIgnoreCase(network)){
			MTNRequest mtnDebitCustomerRequest = new MTNRequest();
			BigDecimal amount = new BigDecimal(debitCustomerRequest.getAmount()).setScale(2, BigDecimal.ROUND_HALF_EVEN);
			String amountInPesewas = amount.toString().replace(".", "");
	        log.info("the amount in pesewas: {}",amountInPesewas);
			mtnDebitCustomerRequest.setAmount(amountInPesewas.toString());
			mtnDebitCustomerRequest.setMsisdn(debitCustomerRequest.getMsisdn());
			mtnDebitCustomerRequest.setProcessingNumber(TransactionIdGenerator.nextId().toString());
			MTNResponse mtnDebitCustomerResponse = mtnMoMoService.mtnDebitCustomer(debitCustomerRequest, mtnDebitCustomerRequest,foundProductRecord);
			log.info("***********the response :{}",mtnDebitCustomerResponse);
			if(AppConstants.MTN_SUCCESS.equalsIgnoreCase(mtnDebitCustomerResponse.getResponseCode())){
				debitCustomerResponse.setResponseCode(AppConstants.PENDING);
				debitCustomerResponse.setResponseMessage(AppConstants.PENDING_MESSAGE);
			}else{
				debitCustomerResponse.setResponseCode(mtnDebitCustomerResponse.getResponseCode());
				debitCustomerResponse.setResponseMessage(mtnDebitCustomerResponse.getResponseMessage());
			}
		}else if(AppConstants.TIGO.equalsIgnoreCase(network)){
			TigoDebitCustomerRequest tigoDebitCustomerRequest = new TigoDebitCustomerRequest();
			tigoDebitCustomerRequest.setAmount(debitCustomerRequest.getAmount());
			tigoDebitCustomerRequest.setApiKey(debitCustomerRequest.getApiKey());
			tigoDebitCustomerRequest.setCustMsisdn(debitCustomerRequest.getMsisdn());
			tigoDebitCustomerRequest.setItemName(debitCustomerRequest.getNarration());
			tigoDebitCustomerRequest.setRefNo(debitCustomerRequest.getRefNo());
			TigoDebitCustomerResponse tigoDebitCustomerResponse = tigoCashService.tigoDebitCustomer(tigoDebitCustomerRequest);
			log.info("***********the response :{}",tigoDebitCustomerResponse);
			if(AppConstants.PENDING.equalsIgnoreCase(tigoDebitCustomerResponse.getResponseCode())){
				debitCustomerResponse.setResponseCode(AppConstants.PENDING);
				debitCustomerResponse.setResponseMessage(AppConstants.PENDING_MESSAGE);
			}else{
				debitCustomerResponse.setResponseCode(tigoDebitCustomerResponse.getResponseCode());
				debitCustomerResponse.setResponseMessage(tigoDebitCustomerResponse.getResponseMessage());
			}
			dbLogger.logTransactionLogs(debitCustomerRequest, debitCustomerResponse);
		}else if(AppConstants.VODAFONE.equalsIgnoreCase(network)){
			VodafoneDebitCustomerRequest vodafoneDebitCustomerRequest = new VodafoneDebitCustomerRequest();
			vodafoneDebitCustomerRequest.setAmount(debitCustomerRequest.getAmount());
			vodafoneDebitCustomerRequest.setApiKey(debitCustomerRequest.getApiKey());
			vodafoneDebitCustomerRequest.setMsisdn(debitCustomerRequest.getMsisdn());
			vodafoneDebitCustomerRequest.setRefNo(debitCustomerRequest.getRefNo());
			vodafoneDebitCustomerRequest.setVoucher(debitCustomerRequest.getVoucher());
			VodafoneDebitCustomerResponse vodafoneDebitCustomerResponse = vodafoneCashService.vodafoneDebitCustomer(vodafoneDebitCustomerRequest);
			log.info("***********the response :{}",vodafoneDebitCustomerResponse);
			if(AppConstants.PENDING.equalsIgnoreCase(vodafoneDebitCustomerResponse.getResponseCode())){
				debitCustomerResponse.setResponseCode(AppConstants.PENDING);
				debitCustomerResponse.setResponseMessage(AppConstants.PENDING);
			}else{
				debitCustomerResponse.setResponseCode(vodafoneDebitCustomerResponse.getResponseCode());
				debitCustomerResponse.setResponseMessage(vodafoneDebitCustomerResponse.getResponseMessage());
			}
			dbLogger.logTransactionLogs(debitCustomerRequest, debitCustomerResponse);
		}
		
//		dbLogger.logTransactionLogs(debitCustomerRequest, debitCustomerResponse);
		return debitCustomerResponse;
	}
	
	public String callback(String requestXml, String merchantId, String productId) {
		TransactionLogs newTransactionLogs = new TransactionLogs();
		PaymentsDto paymentsDto = soapCallbackProcessor.processCallback(requestXml);
		paymentsDto.setProductId(productId);
		
		String processingNumber = paymentsDto.getProcessingNumber();
		String statusCode = paymentsDto.getStatusCode();
		
		TransactionLogs foundTransactionLogs = transactionLogRepository.findByProcessingNumber(processingNumber);
//		newTransactionLogs.setAcctRef(paymentsDto.getThirdPartyAcctRef());
		newTransactionLogs.setMomTransactionId(paymentsDto.getMomTransactionId());
		newTransactionLogs.setResponseCode(paymentsDto.getStatusCode());
		newTransactionLogs.setResponseMessage(paymentsDto.getStatusDesc());
		
		if(StringUtils.equalsIgnoreCase(ResponseCode.TARGET_AUTHORIZATION_ERROR, statusCode)) {
			try {
				newTransactionLogs.setStatus(Status.FAILED);
				return generalUtils.setupRequestPaymentCompletedResponse(statusCode);
			} catch (Exception e) {
				log.error("error {}", e);
			}
		}else if(!StringUtils.equalsIgnoreCase(ResponseCode.CODE_01_OK, statusCode)) {
			newTransactionLogs.setStatus(Status.FAILED);
			return generalUtils.setupRequestPaymentCompletedResponse(statusCode);
		}
		
		newTransactionLogs.setStatus(Status.SUCCESS);
		dbLogger.updateTransactionLogs(foundTransactionLogs, newTransactionLogs);
//		paymentsDto.setTransactionId(foundTransactionLogs.getTransactionId());
		paymentRepository.save(paymentsDto);
		
		String message = JsonUtility.toJson(foundTransactionLogs);
		rabbitMQSubscriber.processMessage(message);
		log.info("after convert and send for payment callback" );

		return generalUtils.setupRequestPaymentCompletedResponse(ResponseCode.CODE_01_OK);
	}
	
}

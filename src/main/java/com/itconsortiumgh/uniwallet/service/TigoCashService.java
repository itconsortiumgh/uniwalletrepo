package com.itconsortiumgh.uniwallet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.uniwallet.model.tigo.TigoCreditCustomerRequest;
import com.itconsortiumgh.uniwallet.model.tigo.TigoCreditCustomerResponse;
import com.itconsortiumgh.uniwallet.model.tigo.TigoDebitCustomerRequest;
import com.itconsortiumgh.uniwallet.model.tigo.TigoDebitCustomerResponse;
import com.itconsortiumgh.uniwallet.properties.ApplicationProperties;

@Service
public class TigoCashService {
	@Autowired
	RestTemplate restTemplate;
	@Autowired 
	ApplicationProperties applicationProperties;
	
	public TigoCreditCustomerResponse tigoCreditCustomer(TigoCreditCustomerRequest request){
		String url = applicationProperties.getTigoCreditCustomerUrl();
		return restTemplate.postForObject(url, request, TigoCreditCustomerResponse.class);
	}
	
	public TigoDebitCustomerResponse tigoDebitCustomer(TigoDebitCustomerRequest request){
		String url = applicationProperties.getTigoDebitCustomerUrl();
		return restTemplate.postForObject(url, request, TigoDebitCustomerResponse.class);
	}
}

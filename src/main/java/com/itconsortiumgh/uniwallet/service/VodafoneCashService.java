package com.itconsortiumgh.uniwallet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.uniwallet.model.vodafone.VodafoneCreditCustomerRequest;
import com.itconsortiumgh.uniwallet.model.vodafone.VodafoneCreditCustomerResponse;
import com.itconsortiumgh.uniwallet.model.vodafone.VodafoneDebitCustomerRequest;
import com.itconsortiumgh.uniwallet.model.vodafone.VodafoneDebitCustomerResponse;
import com.itconsortiumgh.uniwallet.properties.ApplicationProperties;

@Service
public class VodafoneCashService {
	@Autowired
	RestTemplate restTemplate;
	@Autowired 
	ApplicationProperties applicationProperties;
	
	public VodafoneCreditCustomerResponse vodafoneCreditCustomer(VodafoneCreditCustomerRequest request){
		String url = applicationProperties.getVodafoneCreditCustomerUrl();
		return restTemplate.postForObject(url, request, VodafoneCreditCustomerResponse.class);
	}
	
	public VodafoneDebitCustomerResponse vodafoneDebitCustomer(VodafoneDebitCustomerRequest request){
		String url = applicationProperties.getVodafoneDebitCustomerUrl();
		return restTemplate.postForObject(url, request, VodafoneDebitCustomerResponse.class);
	}
	
}

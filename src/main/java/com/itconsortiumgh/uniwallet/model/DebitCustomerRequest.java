package com.itconsortiumgh.uniwallet.model;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@Component
public class DebitCustomerRequest {
	@JsonProperty(required=true)
	private String refNo;
	@JsonProperty(required=true)
	private String merchantId;
	@JsonProperty(required=true)
	private String productId;
	@JsonProperty(required=true)
	private String msisdn;
	@JsonProperty(required=true)
	private String network;
	@JsonProperty(required=true)
	private String amount;
	@JsonProperty(required=true)
	private String apiKey;
	private String narration;
	private String voucher;
	
}

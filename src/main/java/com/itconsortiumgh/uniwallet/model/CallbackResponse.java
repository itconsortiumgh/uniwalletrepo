package com.itconsortiumgh.uniwallet.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class CallbackResponse {
	private String responseCode;
	private String responseMessage;
}

package com.itconsortiumgh.uniwallet.model;

public class ResponseCode {
	public static final String CODE_01_OK = "01";
	public static final String CODE_101_INSUFFICIENT_FUNDS = "101";
	public static final String CODE_100_GENERAL_FAILURE = "101";
	public static final String TARGET_AUTHORIZATION_ERROR = "TARGET_AUTHORIZATION_ERROR";

	public static final String GENERAL_SUCCESS = "01";
	
	public static final String PAYMENT_PROCESS = "03";
	public static final String MTN_SUCCESS = "08";
	public static final String PAYMENT_FAIL = "100";
	
	public static final String MERCHANT_CREATE_FAIL = "101";
	public static final String MERCHANT_RETRIEVE_FAIL = "102";
	public static final String MERCHANT_UPDATE_FAIL = "103";
	public static final String MERCHANT_DELETE_FAIL = "104";
	
	public static final String TRANS_RETRIEVE_FAIL = "105";
	public static final String INVALID_REQUEST = "106";
	public static final String SERVER_FAIL = "107";
	public static final String NOT_FOUND = "108";
	public static final String DATA_VIOLATION = "109";
	public static final String DUPLICATE_TRANSACTION = "110";
}

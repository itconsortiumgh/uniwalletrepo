package com.itconsortiumgh.uniwallet.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class ProductRequest {
	private String productId;
	private String productName;
	private String productDesc;
	private String productCode;
	private String requestPaymentCallbackUrl;
	private String spPassword;
	private String spId;
	private String serviceId;
	private String imsiNum;
	private String balance;
	private String minDueAmount;
	private String narration;
}

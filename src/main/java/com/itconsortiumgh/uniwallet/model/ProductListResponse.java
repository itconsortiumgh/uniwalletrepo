package com.itconsortiumgh.uniwallet.model;

import java.util.List;

import com.itconsortiumgh.uniwallet.model.db.ProductTable;

import lombok.Data;

@Data
public class ProductListResponse {
	private String responseCode;
	private String responseMessage;
	private List<ProductTable> productsList;
}

package com.itconsortiumgh.uniwallet.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class DebitCustomerCallbackResponse {
	private String responseCode;
	private String responseMessage;
}

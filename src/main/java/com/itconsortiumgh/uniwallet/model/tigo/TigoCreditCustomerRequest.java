package com.itconsortiumgh.uniwallet.model.tigo;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class TigoCreditCustomerRequest {
	private String refNo;
	private String msisdn;
	private String amount;
	private String apiKey;
}

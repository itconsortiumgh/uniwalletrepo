package com.itconsortiumgh.uniwallet.model.tigo;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class TigoDebitCustomerRequest {
	private String refNo;
	private String custMsisdn;
	private String itemName;
	private String amount;
	private String apiKey;
}

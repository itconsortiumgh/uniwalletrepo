package com.itconsortiumgh.uniwallet.model.tigo;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class TigoCreditCustomerResponse {
	private String responseCode;
	private String responseMessage;
}

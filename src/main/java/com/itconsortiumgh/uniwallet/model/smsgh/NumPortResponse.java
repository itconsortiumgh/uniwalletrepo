package com.itconsortiumgh.uniwallet.model.smsgh;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class NumPortResponse {
	private String network;
}

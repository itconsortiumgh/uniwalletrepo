package com.itconsortiumgh.uniwallet.model.smsgh;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@Component
public class CurrentCarrier {
	@JsonProperty(value="network_code")
	private String networkCode;
	@JsonProperty(value="country")
	private String country;
	@JsonProperty(value="network_type")
	private String networkType;
	@JsonProperty(value="name")
	private String name;
}

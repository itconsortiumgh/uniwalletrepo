package com.itconsortiumgh.uniwallet.model.smsgh;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@Component
public class NetworkResponse {
	private String status;
	@JsonProperty(value="international_format")
	private String internationalFormat;
	@JsonProperty(value="local_number")
	private LocalNumber localNumber;
	@JsonProperty(value="current_carrier")
	private CurrentCarrier currentCarrier;
	@JsonProperty(value="original_carrier")
	private OriginalCarrier originalCarrier;
	private String ported;

}

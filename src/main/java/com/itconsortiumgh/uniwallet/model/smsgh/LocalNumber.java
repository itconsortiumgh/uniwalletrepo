package com.itconsortiumgh.uniwallet.model.smsgh;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@Component
public class LocalNumber {
	@JsonProperty(value="country_code")
	private String countryCode;
	@JsonProperty(value="country_name")
	private String countryName;
	@JsonProperty(value="country_prefix")
	private int countryPrefix;
	@JsonProperty(value="national_format")
	private String nationalFormat;
}

package com.itconsortiumgh.uniwallet.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class HttpResponseCode {
	public static final int OK = 200;

}

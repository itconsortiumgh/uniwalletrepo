package com.itconsortiumgh.uniwallet.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class AppConstants {
	public static final String MTN = "MTN";
	public static final String TIGO = "TIGO";
	public static final String VODAFONE = "VODAFONE";
	public static final String MTN_SUCCESS = "1000";
	public static final String PENDING = "03"; 
	public static final String PENDING_MESSAGE = "Processing payment";
	public static final String SUCCESS = "01";
	public static final String SUCCESS_MESSAGE = "Payment successful";
}

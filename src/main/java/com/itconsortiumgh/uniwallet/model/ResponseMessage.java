package com.itconsortiumgh.uniwallet.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class ResponseMessage {
	public static final String PAYMENT_PROCESS = "Processing payment";
	public static final String PAYMENT_SUCCESS = "Payment successful";
	public static final String PAYMENT_FAIL = "Payment failed";
	public static final String MERCHANT_CREATE_SUCCESS = "Merchant creation successful";
	public static final String MERCHANT_CREATE_FAIL = "Merchant creation failure";
	public static final String MERCHANT_RETRIEVE_SUCCESS = "Merchant retrieval successful";
	public static final String MERCHANT_RETRIEVE_FAIL = "Merchant retrieval failure";
	public static final String MERCHANT_UPDATE_SUCCESS = "Merchant update successful";
	public static final String MERCHANT_UPDATE_FAIL = "Merchant update failure";
	public static final String MERCHANT_DELETE_SUCCESS = "Merchant deletion successful";
	public static final String MERCHANT_DELETE_FAIL = "Merchant deletion failure";
	
	public static final String PRODUCT_CREATE_SUCCESS = "Product creation successful";
	public static final String PRODUCT_CREATE_FAIL = "Product creation failure";
	public static final String PRODUCT_RETRIEVE_SUCCESS = "Product retrieval successful";
	public static final String PRODUCT_RETRIEVE_FAIL = "Product retrieval failure";
	public static final String PRODUCT_UPDATE_SUCCESS = "Product update successful";
	public static final String PRODUCT_UPDATE_FAIL = "Product update failure";
	public static final String PRODUCT_DELETE_SUCCESS = "Product deletion successful";
	public static final String PRODUCT_DELETE_FAIL = "Product deletion failure";
	
	public static final String TRANS_RETRIEVE_SUCCESS = "Transactions retrieval successful";
	public static final String TRANS_RETRIEVE_FAIL = "Transactions retrieval failure";
	public static final String INVALID_REQUEST = "Invalid Request";
	public static final String SERVER_FAIL = "Connection is closed";
	public static final String NOT_FOUND = "Record not found";
	public static final String DATA_VIOLATION = "Data Integrity Violation";
	
	public static final String MANDATE_CREATE_SUCCES = "Mandate creation successful";
	public static final String GET_PACKETS_SUCCESS = "Packets retrieval successful";
	public static final String MANDATE_UPDATE_SUCCESS = "Mandate update successful";
	public static final String USER_APPROVE_SUCCESS = "User pre-approval successful";
	public static final String DEBIT_CUSTOMER_SUCCESS = "Customer Debit successful";
	
	public static final String MANDATE_CREATE_FAIL = "Mandate creation failed";
	public static final String GET_PACKETS_FAIL = "Packets retrieval failed";
	public static final String MANDATE_UPDATE_FAIL = "Mandate update failure";
	
	public static final String MERCHANT_NOT_FOUND = "Merchant not found";
	public static final String PRODUCT_NOT_FOUND = "Product not found";
	public static final String USER_NOT_VALID = "User not valid";
	public static final String USER_NOT_APPROVED = "User not approved";
	public static final String DUPLICATE_TRANSACTION = "Duplicate transaction";
	
	
}

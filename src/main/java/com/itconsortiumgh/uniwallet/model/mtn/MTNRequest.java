package com.itconsortiumgh.uniwallet.model.mtn;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class MTNRequest {
	private String processingNumber;
	private String imsiNum;
	private String msisdn;
	private String spId;
	private String spPassword;
	private String serviceId;
	private String amount;
	private String acctRef;
	private String balance;
	private String minDueAmount;
	private String narration;
}

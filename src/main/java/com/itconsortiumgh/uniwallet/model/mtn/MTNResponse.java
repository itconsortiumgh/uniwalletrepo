package com.itconsortiumgh.uniwallet.model.mtn;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@Component
public class MTNResponse {
	@JsonInclude(Include.NON_NULL)
	private String momTransactionId;
	private String responseCode;
	private String responseMessage;
	@JsonInclude(Include.NON_NULL)
	private String subscriberID;
	@JsonInclude(Include.NON_NULL)
	private String amount;
	@JsonInclude(Include.NON_NULL)
	private String thirdpartyAccountRef;
	@JsonInclude(Include.NON_NULL)
	private String processingNumber;
}

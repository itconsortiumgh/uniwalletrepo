package com.itconsortiumgh.uniwallet.model.db;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Networks {
	private String network;
}	

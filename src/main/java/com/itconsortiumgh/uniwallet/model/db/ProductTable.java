package com.itconsortiumgh.uniwallet.model.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Component
@Table
@Entity
public class ProductTable implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(length = 20,unique=true)
	private String productId;
	@Column(length=50)
	private String productName;
	@Column(length=50)
	private String productDesc;
	@Column(length=50,unique=true)
	private String productCode;
	@Column
	private String requestPaymentCallbackUrl;
	@Column(length=50)
	private String spPassword;
	@Column(length=50)
	private String spId;
	@Column(length=50)
	private String serviceId;
	@Column(length = 50)
	private String apiKey;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "merchantId",referencedColumnName = "merchantId")
	@JsonIgnore
	private MerchantTable merchant;
}

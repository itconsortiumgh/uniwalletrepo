package com.itconsortiumgh.uniwallet.model.db;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@Table
@Entity
public class PaymentsDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(length=20)
	private String merchantId;
	@Column(length=20)
	private String productId;
//	@Column
//	private String thirdPartyAcctRef;
	@Column(length=50,unique=true)
	private String processingNumber;
	@Column(length=50,unique=true)
	private String momTransactionId;
	@Column(length=50)
	private String statusCode;
	@Column(length=50)
	private String statusDesc;
	@Column(length=20)
	private String status;
//	@Column(length=50,unique=true)
//	private String transactionId;
	@Column(precision = 12, scale = 2)
	private BigDecimal amount;
	@Column(precision = 12, scale = 2)
	private BigDecimal charge;
	@Column(length = 50)
	private String invoiceNo;
	@Column(length = 50)
	private String msisdn;
	@Column(length=50)
	private String paymentDesc;
}

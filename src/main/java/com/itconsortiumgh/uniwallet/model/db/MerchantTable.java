package com.itconsortiumgh.uniwallet.model.db;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Table
@Entity
@Component
//@IdClass(MerchantId.class)
public class MerchantTable implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(length = 20, unique=true)
	private String merchantId;
	@Column(length = 100,unique=true)
	private String merchantName;
	@Column
	private String address;
	@Column(length = 15)
	private String supportMobile;
	@Column(length = 100)
	private String supportEmail;
	@Column(length = 50)
	private String merchantCategory;
	@Column(length=5)
	private String active;
	@OneToMany(mappedBy="merchant", cascade=CascadeType.ALL)
	@JsonIgnore
	private List<ProductTable> products;
	
}

package com.itconsortiumgh.uniwallet.model.db;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Table
@Data
@Entity
@Slf4j
public class TransactionLogs {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	// START TRANSACTIONAL DATA
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;
	@Column(length = 50)
	private String merchantId;
	@Column(length = 50)
	private String productId;
	@Column(length = 50)
	private String productName;
	@Column(length = 50)
	private String msisdn;
	@Column(precision = 12, scale = 2)
	private BigDecimal amount;
	@Column(precision = 12, scale = 2)
	private BigDecimal charge;
//	@Column(length = 50,unique=true)
//	private String transactionId;
	@Column(length = 50,unique=true)
	private String thirdPartyTransactionId;
	@Column(length = 50)
	private String invoiceNo;
	@Column(length = 50)
	private String responseCode;
	@Column(length = 50)
	private String responseMessage;
	@Column(length = 50)
	private String momTransactionId;
	@Column(length = 50)
	private String subscriberID;
	@Column(length = 50,unique=true)
	private String processingNumber;
	@Column(length=50)
	private String paymentDesc;
//	@Column
//	private String acctRef;
	@Column(length = 50)
	private String apiKey;
	@Column(length=10)
	private String network;
	@Column(length = 50)
	private String narration;
	@Column(length = 20)
	private String status;
}

package com.itconsortiumgh.uniwallet.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class DebitCustomerCallbackRequest {
	private String refNo;
	private String msisdn;
	private String merchantId;
	private String productId;
	private String amount;
	private String responseCode;
	private String responseMessage;
}

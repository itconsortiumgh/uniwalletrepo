package com.itconsortiumgh.uniwallet.model.vodafone;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class VodafoneDebitCustomerRequest {
	private String refNo;
	private String msisdn;
	private String amount;
	private String voucher;
	private String apiKey;
}

package com.itconsortiumgh.uniwallet.model.vodafone;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class VodafoneCreditCustomerRequest {
	private String refNo;
	private String msisdn;
	private String amount;
	private String apiKey;
}

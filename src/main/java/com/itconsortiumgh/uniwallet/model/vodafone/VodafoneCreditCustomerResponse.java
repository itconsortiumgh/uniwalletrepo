package com.itconsortiumgh.uniwallet.model.vodafone;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class VodafoneCreditCustomerResponse {
	private String responseCode;
	private String responseMessage;
}

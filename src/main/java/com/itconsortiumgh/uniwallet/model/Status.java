package com.itconsortiumgh.uniwallet.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Status {
	public static final String COMPLETED = "Completed";
	public static final String CREATED = "Created";
	public static final String PENDING = "Pending";
	public static final String UPDATED = "Updated";
	public static final String TRUE = "true";
	public static final String APPROVED = "APPROVED";
	public static final String CALLBACK = "Callback";
	public static final String AUTHORISED = "Authorised";
	public static final String FAILED = "Failed";
	public static final String SUCCESS = "Succes";
}

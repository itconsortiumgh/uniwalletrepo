package com.itconsortiumgh.uniwallet.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.itconsortiumgh.uniwallet.model.db.MerchantTable;
import com.itconsortiumgh.uniwallet.model.db.ProductTable;

import lombok.Data;

@Data
public class CRUDResponse {
	private String responseCode;
	private String responseMessage;
	@JsonInclude(Include.NON_NULL)
	private MerchantTable merchant;
	@JsonInclude(Include.NON_NULL)
	private ProductTable product;
}

package com.itconsortiumgh.uniwallet.amqp;

public interface MessagePublisher {

    void publish(final String message);
}
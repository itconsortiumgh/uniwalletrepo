package com.itconsortiumgh.uniwallet.amqp;

import java.math.BigDecimal;
import java.util.concurrent.CountDownLatch;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.itconsortiumgh.uniwallet.model.DebitCustomerCallbackRequest;
import com.itconsortiumgh.uniwallet.model.DebitCustomerCallbackResponse;
import com.itconsortiumgh.uniwallet.model.ResponseCode;
import com.itconsortiumgh.uniwallet.model.Status;
import com.itconsortiumgh.uniwallet.model.db.ProductTable;
import com.itconsortiumgh.uniwallet.model.db.TransactionLogs;
import com.itconsortiumgh.uniwallet.properties.ApplicationProperties;
import com.itconsortiumgh.uniwallet.repository.ProductTableRepository;
import com.itconsortiumgh.uniwallet.utils.DBLogger;
import com.itconsortiumgh.uniwallet.utils.GeneralUtils;
import com.itconsortiumgh.uniwallet.utils.JsonUtility;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RabbitMQSubscriber implements MessageListener{
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	GeneralUtils generalUtils;
	@Autowired
	ProductTableRepository productTableRepository;
	@Autowired
	DBLogger dbLogger;

    private CountDownLatch latch = new CountDownLatch(1);
    
	@Override
	public void onMessage(Message incomingMessage) {
		// TODO Auto-generated method stub
		Long delay1 = new Long(applicationProperties.getDelayBeforePush().trim());
		long delay  = delay1.longValue();
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String message = new String(incomingMessage.getBody());
		processMessage(message);
	}

    public void processMessage(String message) {
        System.out.println("Received <" + message + ">");
        DebitCustomerCallbackRequest debitCustomerCallbackRequest = new DebitCustomerCallbackRequest();
        TransactionLogs newTransactionLogs = new TransactionLogs();
        TransactionLogs foundTransactionLogs = JsonUtility.fromJson(message, TransactionLogs.class);
        ProductTable foundProductRecord = productTableRepository.findByMerchantIdAndProductId(foundTransactionLogs.getMerchantId(), foundTransactionLogs.getProductId());

        if(foundProductRecord != null){
        	String url = foundProductRecord.getRequestPaymentCallbackUrl();
        	
        	debitCustomerCallbackRequest.setMerchantId(foundTransactionLogs.getMerchantId());
        	debitCustomerCallbackRequest.setProductId(foundTransactionLogs.getProductId());
        	debitCustomerCallbackRequest.setMsisdn(foundTransactionLogs.getMsisdn());
        	debitCustomerCallbackRequest.setRefNo(foundTransactionLogs.getThirdPartyTransactionId());
        	debitCustomerCallbackRequest.setAmount(foundTransactionLogs.getAmount().toString());
//        	debitCustomerCallbackRequest.setResponseCode(ResponseCode.CODE_01_OK);
        	debitCustomerCallbackRequest.setResponseCode(foundTransactionLogs.getResponseCode());
        	debitCustomerCallbackRequest.setResponseMessage(foundTransactionLogs.getResponseMessage());
        	
        	//Call third party back
        	log.info("***************Hitting {} for a response****************");
        	log.info("***************the request going : {}",debitCustomerCallbackRequest);
        	DebitCustomerCallbackResponse debitCustomerCallbackResponse = restTemplate.postForObject(url, debitCustomerCallbackRequest, DebitCustomerCallbackResponse.class);
        	log.info("***************the response coming : {}",debitCustomerCallbackResponse);
        	if(ResponseCode.CODE_01_OK.equalsIgnoreCase(debitCustomerCallbackResponse.getResponseCode())){
        		newTransactionLogs.setStatus(Status.COMPLETED);
        		
        		//call genpay
    			String genPayUrl = applicationProperties.getGenpayUrl();
    			String merchantId = foundTransactionLogs.getMerchantId();
    			String invoiceNo = foundTransactionLogs.getProductId();
    			String amount = foundTransactionLogs.getAmount().toString();
    			String amountInPesewas = amount.replace(".", "");
    			String fee = applicationProperties.getCharge();
    			String msisdn = foundTransactionLogs.getMsisdn();
    			String ova = foundProductRecord.getServiceId();
    			String paymentDesc = foundTransactionLogs.getResponseMessage();
    			String acctRef = generalUtils.encodeAccountRef(merchantId, amountInPesewas, fee, invoiceNo, msisdn, ova, paymentDesc);
    			
    			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(genPayUrl)
    					.queryParam("src", applicationProperties.getScr())
    					.queryParam("service", applicationProperties.getService())
    					.queryParam("op", applicationProperties.getOp())
    					.queryParam("acctRef",acctRef)
    					.queryParam("thirdPartyTransactionID", foundTransactionLogs.getThirdPartyTransactionId())
    					.queryParam("statusCode", foundTransactionLogs.getResponseCode())
    					.queryParam("statusDesc", foundTransactionLogs.getResponseMessage())
    					.queryParam("ova", foundProductRecord.getServiceId())
    					.queryParam("fundamoTransactionID", foundTransactionLogs.getMomTransactionId());
    			
    			log.info("*********Hitting {} for a response***********",genPayUrl);
    			String response = restTemplate.getForObject(builder.build().encode().toUri(), String.class);
    			log.info("*********the response: {}",response);
        	}else{
        		newTransactionLogs.setStatus(Status.CALLBACK);
        	}
        	dbLogger.updateTransactionLogs(foundTransactionLogs, newTransactionLogs);
        }
        
//        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }


}

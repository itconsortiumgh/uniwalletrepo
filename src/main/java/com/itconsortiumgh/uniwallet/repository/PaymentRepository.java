package com.itconsortiumgh.uniwallet.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itconsortiumgh.uniwallet.model.db.PaymentsDto;

@Repository
public interface PaymentRepository extends CrudRepository<PaymentsDto, Long> {

}

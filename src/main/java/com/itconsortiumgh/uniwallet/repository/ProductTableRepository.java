package com.itconsortiumgh.uniwallet.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itconsortiumgh.uniwallet.model.db.ProductTable;

@Repository
public interface ProductTableRepository extends CrudRepository<ProductTable, Long>{
	ProductTable findByProductId(String productId);
	ProductTable findByMerchantIdAndProductId(String merchantId, String productId);
	ProductTable findByMerchantIdAndProductIdAndApiKey(String merchantId, String productId, String apiKey);
	ProductTable findByMerchantIdAndApiKey(String merchantId, String apiKey);
	ProductTable findByProductIdAndApiKey(String productId, String apiKey);
	ProductTable findByApiKey(String apiKey);
	List<ProductTable> findByMerchantId(String merchantId);
}

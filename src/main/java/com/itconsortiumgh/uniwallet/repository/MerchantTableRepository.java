package com.itconsortiumgh.uniwallet.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itconsortiumgh.uniwallet.model.db.MerchantTable;

@Repository
public interface MerchantTableRepository extends CrudRepository<MerchantTable, Long>{
	MerchantTable findByMerchantId(String merchantId);
}

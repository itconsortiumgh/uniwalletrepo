package com.itconsortiumgh.uniwallet.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itconsortiumgh.uniwallet.model.db.TransactionLogs;

@Repository
public interface TransactionLogRepository extends CrudRepository<TransactionLogs, Long> {
	public TransactionLogs findByProcessingNumber(String processingNumber);
	TransactionLogs findByThirdPartyTransactionId(String thirdPartyTransactionId);
}

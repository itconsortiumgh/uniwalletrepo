package com.itconsortiumgh.uniwallet.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itconsortiumgh.uniwallet.model.db.Network;


@Repository
public interface NetworkRepository extends CrudRepository<Network,Long>{
	@Query(value="Select network from network",nativeQuery=true)
	public List<String> getNetworks();
}

package com.itconsortiumgh.uniwallet.properties;

import javax.persistence.Column;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties
public class ApplicationProperties {
	private String mtnDebitCustomerUrl;
	private String mtnCreditCustomerUrl;
	private String imsiNum;
	private String spId;
	private String spPassword;
	private String serviceId;
	private String acctRef;
	private String balance;
	private String minDueAmount;
	private String narration;
	private String tigoCreditCustomerUrl;
	private String tigoDebitCustomerUrl;
	private String vodafoneCreditCustomerUrl;
	private String vodafoneDebitCustomerUrl;
	
	private String numberPortabilityUrl;
	private String smsghUsername;
	private String smsghPassword;
	
	private String uniQueueName;
	private String uniQueueExchange;
	private String receiveMessage;
	private String delayBeforePush;
	
	private String ova;
	private String scr;
	private String service;
	private String op;
	private String genpayUrl;
	
	private String charge;
	
}

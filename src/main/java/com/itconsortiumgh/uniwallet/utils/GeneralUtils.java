package com.itconsortiumgh.uniwallet.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Base64;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.itconsortiumgh.uniwallet.model.ResponseCode;
import com.itconsortiumgh.uniwallet.model.db.PaymentsDto;
import com.itconsortiumgh.uniwallet.model.db.TransactionLogs;

import lombok.extern.slf4j.Slf4j;


@Component
@Slf4j
public class GeneralUtils {
	@Autowired
	RestTemplate restTemplate;

	public String setupRequestPaymentCompletedResponse(String statusCode) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DOMImplementation impl = builder.getDOMImplementation();

		Document message = impl.createDocument(null, null, null);
		Element envelope = message.createElement("S:Envelope");
		envelope.setAttribute("xmlns:S", "http://schemas.xmlsoap.org/soap/envelope/");
		message.appendChild(envelope);

		Element body = message.createElement("S:Body");
		envelope.appendChild(body);

		Element requestPaymentResponse = message.createElement("ns3:requestPaymentCompletedResponse");
		requestPaymentResponse.setAttribute("xmlns:ns2", "http://www.huawei.com.cn/schema/common/v2_1");
		requestPaymentResponse.setAttribute("xmlns:ns3", "http://www.csapi.org/schema/momopayment/local/v1_0");
		body.appendChild(requestPaymentResponse);

		Element ns3Result = message.createElement("ns3:result");
		requestPaymentResponse.appendChild(ns3Result);

		if(StringUtils.equalsIgnoreCase(ResponseCode.CODE_01_OK, statusCode)) {
			Element resultCode = message.createElement("resultCode");
			ns3Result.appendChild(resultCode);
			resultCode.setTextContent("00000000");

			Element resultDescription = message.createElement("resultDescription");
			ns3Result.appendChild(resultDescription);
			resultDescription.setTextContent("success");
		}else {
			Element resultCode = message.createElement("resultCode");
			ns3Result.appendChild(resultCode);
			resultCode.setTextContent("10000000");

			Element resultDescription = message.createElement("resultDescription");
			ns3Result.appendChild(resultDescription);
			resultDescription.setTextContent("failure");
		}

		Element extensionInfo = message.createElement("ns3:extensionInfo");
		requestPaymentResponse.appendChild(extensionInfo);

		Element item = message.createElement("item");
		extensionInfo.appendChild(item);

		Element key = message.createElement("key");
		item.appendChild(key);
		key.setTextContent("?");

		Element value = message.createElement("value");
		item.appendChild(value);
		value.setTextContent("?");

		DocumentToStringXMLConverter docConverter = new DocumentToStringXMLConverter();
		String xml = docConverter.convertDocumentToStringXML(message);
		return xml;
	}

	public String encodeAccountRef(String merchantId, String amount,String fee,String invoiceNo,String msisdn, String ova,String paymentDesc){
		String encodedMsg = "";
		//prepare the message
		String concatString = merchantId+"|"+amount+"|"+fee+"|"+invoiceNo+"|"+msisdn+"|"+ova+"|"+"INV"+""+invoiceNo+""+msisdn+"|"+paymentDesc;
		
		//encode the message
		try {
			encodedMsg = Base64.getEncoder().encodeToString(concatString.getBytes("UTF-8"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return encodedMsg;
	}
	
	public TransactionLogs decodeAccountRef(String acctRef){
		TransactionLogs transactionLogs = new TransactionLogs();
		String decodedAcctRef = "";
		
		byte[] asBytes = Base64.getDecoder().decode(acctRef.trim());
		try {
			decodedAcctRef = new String(asBytes, "utf-8");
			log.info("decoded accountRef {}", decodedAcctRef);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		String accountRefArray[] = decodedAcctRef.split("|");
		String merchantId = accountRefArray[0];
		BigDecimal amount = new BigDecimal(accountRefArray[1]);
		BigDecimal fee = new BigDecimal(accountRefArray[2]);
		String invoiceNo = accountRefArray[3]; 
		String msisdn = accountRefArray[4];
		String ova = accountRefArray[5];
		String junk = accountRefArray[6];
		String paymentDesc = accountRefArray[7];
		
		transactionLogs.setMerchantId(merchantId);
		transactionLogs.setAmount(amount);
		transactionLogs.setCharge(fee);
		transactionLogs.setInvoiceNo(invoiceNo);
		transactionLogs.setMsisdn(msisdn);
		transactionLogs.setPaymentDesc(paymentDesc);
		
		return transactionLogs;
	}
	public PaymentsDto decodeAcctRef(String acctRef){
		PaymentsDto paymentsDto = new PaymentsDto();
		String decodedAcctRef = "";
		
		byte[] asBytes = Base64.getDecoder().decode(acctRef.trim());
		try {
			decodedAcctRef = new String(asBytes, "utf-8");
			log.info("decoded accountRef {}", decodedAcctRef);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		String accountRefArray[] = decodedAcctRef.split("|");
		String merchantId = accountRefArray[0];
		BigDecimal amount = new BigDecimal(accountRefArray[1]);
		BigDecimal fee = new BigDecimal(accountRefArray[2]);
		String invoiceNo = accountRefArray[3]; 
		String msisdn = accountRefArray[4];
		String ova = accountRefArray[5];
		String junk = accountRefArray[6];
		String paymentDesc = accountRefArray[7];
		
		paymentsDto.setMerchantId(merchantId);
		paymentsDto.setAmount(amount);
		paymentsDto.setCharge(fee);
		paymentsDto.setInvoiceNo(invoiceNo);
		paymentsDto.setMsisdn(msisdn);
		paymentsDto.setPaymentDesc(paymentDesc);
		
		return paymentsDto;
	}
	
//	public static void main(String[] args) {
//		GeneralUtils gen = new GeneralUtils();
//		String print = gen.encodeAccountRef("51", "100", "0", "TF2", "233209331457", "schoolfees", "Momo");
//		System.out.println(print);
//	}
}
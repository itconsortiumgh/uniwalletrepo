package com.itconsortiumgh.uniwallet.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class ApiKeyGenerator {
	public static void main(String[] args) {
		System.out.println(ApiKeyGenerator.nextSessionId());
	}
	
	public static String nextSessionId() {
		return RandomStringUtils.random(32, true, true);
	}
}

package com.itconsortiumgh.uniwallet.utils;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.uniwallet.model.CreditCustomerRequest;
import com.itconsortiumgh.uniwallet.model.CreditCustomerResponse;
import com.itconsortiumgh.uniwallet.model.DebitCustomerRequest;
import com.itconsortiumgh.uniwallet.model.DebitCustomerResponse;
import com.itconsortiumgh.uniwallet.model.ProductRequest;
import com.itconsortiumgh.uniwallet.model.Status;
import com.itconsortiumgh.uniwallet.model.db.ProductTable;
import com.itconsortiumgh.uniwallet.model.db.TransactionLogs;
import com.itconsortiumgh.uniwallet.model.mtn.MTNRequest;
import com.itconsortiumgh.uniwallet.model.mtn.MTNResponse;
import com.itconsortiumgh.uniwallet.repository.ProductTableRepository;
import com.itconsortiumgh.uniwallet.repository.TransactionLogRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DBLogger {
	@Autowired
	TransactionLogRepository transactionLogRepository;
	@Autowired
	ProductTableRepository productTableRepository;
	
	public void logTransactionLogs(CreditCustomerRequest creditCustomerRequest,CreditCustomerResponse creditCustomerResponse){
		TransactionLogs transactionLogs = new TransactionLogs();
		
		try {
			transactionLogs.setCreated(new Date());
			transactionLogs.setStatus(Status.CREATED);
			transactionLogs.setMerchantId(creditCustomerRequest.getMerchantId());
			transactionLogs.setProductId(creditCustomerRequest.getProductId());
			transactionLogs.setApiKey(creditCustomerRequest.getApiKey());
			transactionLogs.setThirdPartyTransactionId(creditCustomerRequest.getRefNo());
			transactionLogs.setMsisdn(creditCustomerRequest.getMsisdn());
			transactionLogs.setNetwork(creditCustomerRequest.getNetwork());
			transactionLogs.setAmount(new BigDecimal(creditCustomerRequest.getAmount()));
			transactionLogs.setNarration(creditCustomerRequest.getNarration());
			
			transactionLogs.setResponseCode(creditCustomerResponse.getResponseCode());
			transactionLogs.setResponseMessage(creditCustomerResponse.getResponseMessage());
			
			transactionLogRepository.save(transactionLogs);
			
		} catch (Exception e) {
			// TODO: handle exception
			log.error("the exception :{}",e.getMessage());
		}
	}
	
	public void logTransactionLogs(DebitCustomerRequest debitCustomerRequest,DebitCustomerResponse debitCustomerResponse){
		TransactionLogs transactionLogs = new TransactionLogs();
		
		try {
			transactionLogs.setCreated(new Date());
			transactionLogs.setStatus(Status.CREATED);
			transactionLogs.setMerchantId(debitCustomerRequest.getMerchantId());
			transactionLogs.setProductId(debitCustomerRequest.getProductId());
			transactionLogs.setApiKey(debitCustomerRequest.getApiKey());
			transactionLogs.setThirdPartyTransactionId(debitCustomerRequest.getRefNo());
			transactionLogs.setMsisdn(debitCustomerRequest.getMsisdn());
			transactionLogs.setNetwork(debitCustomerRequest.getNetwork());
			transactionLogs.setAmount(new BigDecimal(debitCustomerRequest.getAmount()));
			transactionLogs.setNarration(debitCustomerRequest.getNarration());
			
			transactionLogs.setResponseCode(debitCustomerResponse.getResponseCode());
			transactionLogs.setResponseMessage(debitCustomerResponse.getResponseMessage());
			
			transactionLogRepository.save(transactionLogs);
			
		} catch (Exception e) {
			// TODO: handle exception
			log.error("the exception :{}",e.getMessage());
		}
	}
	
	public void logTransactionsLogs(CreditCustomerRequest creditCustomerRequest, MTNRequest mtnRequest, MTNResponse mtnResponse){
		TransactionLogs transactionLogs = new TransactionLogs();
		
		try {
			transactionLogs.setCreated(new Date());
			transactionLogs.setStatus(Status.CREATED);
			transactionLogs.setThirdPartyTransactionId(creditCustomerRequest.getRefNo());
			transactionLogs.setMerchantId(creditCustomerRequest.getMerchantId());
			transactionLogs.setProductId(creditCustomerRequest.getProductId());
			transactionLogs.setApiKey(creditCustomerRequest.getApiKey());
			transactionLogs.setNetwork(creditCustomerRequest.getNetwork());
			transactionLogs.setNarration(creditCustomerRequest.getNarration());
			
			transactionLogs.setProcessingNumber(mtnRequest.getProcessingNumber());
			Integer amountInPesewas = Integer.parseInt(mtnRequest.getAmount());
			Integer amount = amountInPesewas/100; 
			transactionLogs.setAmount(new BigDecimal(amount));
			transactionLogs.setMsisdn(mtnRequest.getMsisdn());
//		transactionLogs.setAcctRef(mtnRequest.getAcctRef());
			
			transactionLogs.setMomTransactionId(mtnResponse.getMomTransactionId());
			transactionLogs.setResponseCode(mtnResponse.getResponseCode());
			transactionLogs.setResponseMessage(mtnResponse.getResponseMessage());
			transactionLogs.setSubscriberID(mtnResponse.getSubscriberID());
			
			transactionLogRepository.save(transactionLogs);
			
		} catch (Exception e) {
			// TODO: handle exception
			log.error("the exception :{}",e.getMessage());
		}
	}
	
	public void logTransactionsLogs(DebitCustomerRequest debitCustomerRequest, MTNRequest mtnRequest, MTNResponse mtnResponse){
		TransactionLogs transactionLogs = new TransactionLogs();
		
		try {
			transactionLogs.setCreated(new Date());
			transactionLogs.setStatus(Status.CREATED);
			transactionLogs.setThirdPartyTransactionId(debitCustomerRequest.getRefNo());
			transactionLogs.setMerchantId(debitCustomerRequest.getMerchantId());
			transactionLogs.setProductId(debitCustomerRequest.getProductId());
			transactionLogs.setApiKey(debitCustomerRequest.getApiKey());
			transactionLogs.setNetwork(debitCustomerRequest.getNetwork());
			transactionLogs.setNarration(debitCustomerRequest.getNarration());
			
			transactionLogs.setProcessingNumber(mtnRequest.getProcessingNumber());
			Integer amountInPesewas = Integer.parseInt(mtnRequest.getAmount());
			Integer amount = amountInPesewas/100; 
			transactionLogs.setAmount(new BigDecimal(amount));
			transactionLogs.setMsisdn(mtnRequest.getMsisdn());
//		transactionLogs.setAcctRef(mtnRequest.getAcctRef());
			
			transactionLogs.setMomTransactionId(mtnResponse.getMomTransactionId());
			transactionLogs.setResponseCode(mtnResponse.getResponseCode());
			transactionLogs.setResponseMessage(mtnResponse.getResponseMessage());
			transactionLogs.setSubscriberID(mtnResponse.getSubscriberID());
			
			transactionLogRepository.save(transactionLogs);
			
		} catch (Exception e) {
			// TODO: handle exception
			log.error("the exception : {}",e.getMessage());
		}
	}
	
	public void updateTransactionLogs(TransactionLogs foundTransactionLogs, TransactionLogs newTransactionLogs){
		try {
			foundTransactionLogs.setUpdated(new Date());
			foundTransactionLogs.setStatus(Status.UPDATED);
			if(newTransactionLogs.getMerchantId() != null)
				foundTransactionLogs.setMerchantId(newTransactionLogs.getMerchantId());
			if(newTransactionLogs.getProductId() != null)
				foundTransactionLogs.setProductId(newTransactionLogs.getProductId());
			if(newTransactionLogs.getProductName() != null)
				foundTransactionLogs.setProductName(newTransactionLogs.getProductName());
			if(newTransactionLogs.getAmount() != null)
				foundTransactionLogs.setAmount(newTransactionLogs.getAmount());
			if(newTransactionLogs.getCharge() != null)
				foundTransactionLogs.setCharge(newTransactionLogs.getCharge());
			if(newTransactionLogs.getResponseCode() != null)
				foundTransactionLogs.setResponseCode(newTransactionLogs.getResponseCode());
			if(newTransactionLogs.getResponseMessage() != null)
				foundTransactionLogs.setResponseMessage(newTransactionLogs.getResponseMessage());
			if(newTransactionLogs.getSubscriberID() != null)
				foundTransactionLogs.setSubscriberID(newTransactionLogs.getSubscriberID());
			if(newTransactionLogs.getProcessingNumber() != null)
				foundTransactionLogs.setProcessingNumber(newTransactionLogs.getProcessingNumber());
			if(newTransactionLogs.getPaymentDesc() != null)
				foundTransactionLogs.setPaymentDesc(newTransactionLogs.getPaymentDesc());
//		if(newTransactionLogs.getAcctRef() != null)
//			foundTransactionLogs.setAcctRef(newTransactionLogs.getAcctRef());
			if(newTransactionLogs.getMomTransactionId() != null)
				foundTransactionLogs.setMomTransactionId(newTransactionLogs.getMomTransactionId());
			if(newTransactionLogs.getApiKey() != null)
				foundTransactionLogs.setApiKey(newTransactionLogs.getApiKey());
			if(newTransactionLogs.getNetwork() != null)
				foundTransactionLogs.setNetwork(newTransactionLogs.getNetwork());
			if(newTransactionLogs.getNarration() != null)
				foundTransactionLogs.setNarration(newTransactionLogs.getNarration());
			if(newTransactionLogs.getStatus() != null)
				foundTransactionLogs.setStatus(newTransactionLogs.getStatus());
			
			transactionLogRepository.save(foundTransactionLogs);
		} catch (Exception e) {
			// TODO: handle exception
			log.error("the exception :{}",e.getMessage());
		}
	}
	
	public ProductTable saveProduct(ProductRequest productRequest){
		ProductTable productTable = new ProductTable();
		ProductTable savedProduct = new ProductTable();
		
		try {
			productTable.setProductId(productRequest.getProductId());
			productTable.setProductName(productRequest.getProductName());
			productTable.setProductDesc(productRequest.getProductDesc());
			productTable.setProductCode(productRequest.getProductCode());
			productTable.setRequestPaymentCallbackUrl(productRequest.getRequestPaymentCallbackUrl());
			productTable.setSpPassword(productRequest.getSpPassword());
			productTable.setSpId(productRequest.getSpId());
			productTable.setServiceId(productRequest.getServiceId());
			
			productTable.setApiKey(ApiKeyGenerator.nextSessionId());
			savedProduct = productTableRepository.save(productTable);
		} catch (Exception e) {
			// TODO: handle exception
			log.error("the exception :{}",e.getMessage());
		}
		
		return savedProduct;	
	}
}

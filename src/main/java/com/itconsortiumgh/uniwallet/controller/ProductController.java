package com.itconsortiumgh.uniwallet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.itconsortiumgh.uniwallet.model.CRUDResponse;
import com.itconsortiumgh.uniwallet.model.ProductListResponse;
import com.itconsortiumgh.uniwallet.model.ProductRequest;
import com.itconsortiumgh.uniwallet.model.ResponseCode;
import com.itconsortiumgh.uniwallet.model.ResponseMessage;
import com.itconsortiumgh.uniwallet.model.db.ProductTable;
import com.itconsortiumgh.uniwallet.repository.ProductTableRepository;
import com.itconsortiumgh.uniwallet.utils.DBLogger;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ProductController {
	
	@Autowired
	ProductTableRepository productRepository;
	@Autowired
	DBLogger dbLogger;
	
	@PostMapping("create/product")
	public CRUDResponse createProduct(@RequestBody ProductRequest product){
		log.info("************The Json request : {}",product);
		CRUDResponse crudResponse = new CRUDResponse();
		try {
			if(product != null){
				ProductTable savedProduct = dbLogger.saveProduct(product);
				if(savedProduct != null){
					crudResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
					crudResponse.setResponseMessage(ResponseMessage.PRODUCT_CREATE_SUCCESS);
					crudResponse.setProduct(savedProduct);
				}else{
					crudResponse.setResponseCode(ResponseCode.MERCHANT_CREATE_FAIL);
					crudResponse.setResponseMessage(ResponseMessage.PRODUCT_CREATE_FAIL);
				}
			}else{
				crudResponse.setResponseCode(ResponseCode.INVALID_REQUEST);
				crudResponse.setResponseMessage(ResponseMessage.INVALID_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			crudResponse.setResponseCode(ResponseCode.DATA_VIOLATION);
			crudResponse.setResponseMessage(ResponseMessage.DATA_VIOLATION);
		}
		return crudResponse;
	}
	
	@GetMapping("retrieve/product/id/{id}")
	public CRUDResponse retrieveProduct(@PathVariable String id){
		log.info("************The merchant ID : {}",id);
		CRUDResponse crudResponse = new CRUDResponse();
		ProductTable foundProduct = productRepository.findByProductId(id);
		if(foundProduct != null){
			crudResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
			crudResponse.setResponseMessage(ResponseMessage.PRODUCT_RETRIEVE_SUCCESS);
			crudResponse.setProduct(foundProduct);;
		}else{
			crudResponse.setResponseCode(ResponseCode.NOT_FOUND);
			crudResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
		}
		return crudResponse;
	}
	@GetMapping("retrieve/product/apiKey/{apiKey}")
	public CRUDResponse retrieveProductUsingApiKet(@PathVariable String apiKey){
		log.info("************The apiKey: {}",apiKey);
		CRUDResponse crudResponse = new CRUDResponse();
		ProductTable foundProduct = productRepository.findByApiKey(apiKey);
		if(foundProduct != null){
			crudResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
			crudResponse.setResponseMessage(ResponseMessage.PRODUCT_RETRIEVE_SUCCESS);
			crudResponse.setProduct(foundProduct);;
		}else{
			crudResponse.setResponseCode(ResponseCode.NOT_FOUND);
			crudResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
		}
		return crudResponse;
	}
	
	@PostMapping("update/product")
	public CRUDResponse updateProduct(@RequestBody ProductTable product){
		log.info("************The Json request : {}",product);
		CRUDResponse crudResponse = new CRUDResponse();
		if(product != null){
			ProductTable foundProduct = productRepository.findByProductId(product.getProductId());
			if(foundProduct != null){
				if(product.getProductName() != null)
					foundProduct.setProductName(product.getProductName());
				if(product.getProductDesc() != null)
					foundProduct.setProductDesc(product.getProductDesc());
				if(product.getProductCode() != null)
					foundProduct.setProductCode(product.getProductCode());
//				if(product.getMerchantId() != null)
//					foundProduct.setMerchantId(product.getMerchantId());
//				if(product.getPreApprovalCallbackUrl() != null)
//					foundProduct.setPreApprovalCallbackUrl(product.getPreApprovalCallbackUrl());
				if(product.getRequestPaymentCallbackUrl() != null)
					foundProduct.setRequestPaymentCallbackUrl(product.getRequestPaymentCallbackUrl());
				
				ProductTable savedProduct = productRepository.save(foundProduct);
				if(savedProduct != null){
					crudResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
					crudResponse.setResponseMessage(ResponseMessage.PRODUCT_UPDATE_SUCCESS);
					crudResponse.setProduct(savedProduct);
				}else{
					crudResponse.setResponseCode(ResponseCode.MERCHANT_UPDATE_FAIL);
					crudResponse.setResponseMessage(ResponseMessage.PRODUCT_UPDATE_FAIL);
				}
			}else{
				crudResponse.setResponseCode(ResponseCode.NOT_FOUND);
				crudResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
			}
		}else{
			crudResponse.setResponseCode(ResponseCode.INVALID_REQUEST);
			crudResponse.setResponseMessage(ResponseMessage.INVALID_REQUEST);
		}
		return crudResponse;
	}
	
	@GetMapping("delete/product/id/{id}")
	public CRUDResponse deleteMerchant(@PathVariable String id){
		log.info("************The merchant ID : {}",id);
		CRUDResponse crudResponse = new CRUDResponse();
		ProductTable foundProduct = productRepository.findByProductId(id);
		if(foundProduct != null){
			productRepository.delete(foundProduct);
			crudResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
			crudResponse.setResponseMessage(ResponseMessage.PRODUCT_DELETE_SUCCESS);
			crudResponse.setProduct(foundProduct);
		}else{
			crudResponse.setResponseCode(ResponseCode.NOT_FOUND);
			crudResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
		}
		return crudResponse;
	}
	
	@GetMapping("retrieve/products/merchant/id/{id}")
	public ProductListResponse retrieveTransactions(@PathVariable String id){
		log.info("************The merchant ID : {}",id);
		ProductListResponse transactionListResponse = new ProductListResponse();
		List<ProductTable> foundProductList = productRepository.findByMerchantId(id);
		if(foundProductList != null){
			transactionListResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
			transactionListResponse.setResponseMessage(ResponseMessage.TRANS_RETRIEVE_SUCCESS);
			transactionListResponse.setProductsList(foundProductList);
		}else{
			transactionListResponse.setResponseCode(ResponseCode.TRANS_RETRIEVE_FAIL);
			transactionListResponse.setResponseMessage(ResponseMessage.TRANS_RETRIEVE_FAIL);
		}
		return transactionListResponse;
	}
}

package com.itconsortiumgh.uniwallet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.uniwallet.model.CallbackRequest;
import com.itconsortiumgh.uniwallet.model.CallbackResponse;
import com.itconsortiumgh.uniwallet.model.CreditCustomerRequest;
import com.itconsortiumgh.uniwallet.model.CreditCustomerResponse;
import com.itconsortiumgh.uniwallet.model.DebitCustomerRequest;
import com.itconsortiumgh.uniwallet.model.DebitCustomerResponse;
import com.itconsortiumgh.uniwallet.model.ResponseCode;
import com.itconsortiumgh.uniwallet.model.ResponseMessage;
import com.itconsortiumgh.uniwallet.model.db.MerchantTable;
import com.itconsortiumgh.uniwallet.model.db.ProductTable;
import com.itconsortiumgh.uniwallet.model.db.TransactionLogs;
import com.itconsortiumgh.uniwallet.model.smsgh.NumPortResponse;
import com.itconsortiumgh.uniwallet.repository.MerchantTableRepository;
import com.itconsortiumgh.uniwallet.repository.ProductTableRepository;
import com.itconsortiumgh.uniwallet.repository.TransactionLogRepository;
import com.itconsortiumgh.uniwallet.service.BusinessLogic;
import com.itconsortiumgh.uniwallet.utils.XMLFormatter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class Controller {
	@Autowired
	BusinessLogic businessLogic;
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	MerchantTableRepository merchantTableRepository;
	@Autowired
	ProductTableRepository productTableRepository;
	@Autowired
	TransactionLogRepository transactionLogRepository;

	@GetMapping("/get/network/{msisdn}")
	public NumPortResponse getNetwork(@PathVariable String msisdn) {
		log.info("*************the msisdn: {}", msisdn);
		return businessLogic.getNetwork(msisdn);
	}

	@GetMapping("/get/available/networks")
	public List<String> getNetworks() {
		return businessLogic.getNetworks();
	}

	@PostMapping("/credit/customer")
	public CreditCustomerResponse creditCustomer(@RequestBody CreditCustomerRequest creditCustomerRequest) {
		log.info("*************the request:{}", creditCustomerRequest);
		CreditCustomerResponse creditCustomerResponse = new CreditCustomerResponse();
		if (creditCustomerRequest != null) {
			TransactionLogs duplicateTransaction = transactionLogRepository.findByThirdPartyTransactionId(creditCustomerRequest.getRefNo());
			
			if(duplicateTransaction == null){
				MerchantTable foundMerchant = merchantTableRepository.findByMerchantId(creditCustomerRequest.getMerchantId());
				if (foundMerchant != null) {
					ProductTable foundProductRecord = productTableRepository.findByProductIdAndApiKey(creditCustomerRequest.getProductId(), creditCustomerRequest.getApiKey());
					if (foundProductRecord != null) {
						creditCustomerResponse = businessLogic.creditCustomer(creditCustomerRequest, foundProductRecord);
					} else {
						creditCustomerResponse.setResponseCode(ResponseCode.NOT_FOUND);
						creditCustomerResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
					}
				} else {
					creditCustomerResponse.setResponseCode(ResponseCode.NOT_FOUND);
					creditCustomerResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
				}
			}else{
				creditCustomerResponse.setResponseCode(ResponseCode.DUPLICATE_TRANSACTION);
				creditCustomerResponse.setResponseMessage(ResponseMessage.DUPLICATE_TRANSACTION);
			}
		} else {
			creditCustomerResponse.setResponseCode(ResponseCode.INVALID_REQUEST);
			creditCustomerResponse.setResponseMessage(ResponseMessage.INVALID_REQUEST);
		}
		return creditCustomerResponse;
	}

	@PostMapping("/debit/customer")
	public DebitCustomerResponse debitCustomer(@RequestBody DebitCustomerRequest debitCustomerRequest) {
		log.info("*************the request:{}", debitCustomerRequest);
		DebitCustomerResponse debitCustomerResponse = new DebitCustomerResponse();
		if (debitCustomerRequest != null) {
			TransactionLogs duplicateTransaction = transactionLogRepository.findByThirdPartyTransactionId(debitCustomerRequest.getRefNo());
			
			if(duplicateTransaction == null){
				MerchantTable foundMerchant = merchantTableRepository.findByMerchantId(debitCustomerRequest.getMerchantId());
				if (foundMerchant != null) {
					ProductTable foundProductRecord = productTableRepository.findByProductIdAndApiKey(debitCustomerRequest.getProductId(), debitCustomerRequest.getApiKey());
					if (foundProductRecord != null) {
						debitCustomerResponse = businessLogic.debitCustomer(debitCustomerRequest, foundProductRecord);
					} else {
						debitCustomerResponse.setResponseCode(ResponseCode.NOT_FOUND);
						debitCustomerResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
					}
				} else {
					debitCustomerResponse.setResponseCode(ResponseCode.NOT_FOUND);
					debitCustomerResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
				}
				
			}else{
				debitCustomerResponse.setResponseCode(ResponseCode.DUPLICATE_TRANSACTION);
				debitCustomerResponse.setResponseMessage(ResponseMessage.DUPLICATE_TRANSACTION);
			}
		} else {
			debitCustomerResponse.setResponseCode(ResponseCode.INVALID_REQUEST);
			debitCustomerResponse.setResponseMessage(ResponseMessage.INVALID_REQUEST);
		}
		return debitCustomerResponse;
	}

	@PostMapping(value = "/callback/{merchantId}/{productId}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.TEXT_XML_VALUE })
	public String callback(@PathVariable String merchantId, @PathVariable String productId,
			@RequestBody String sdpRequest, HttpServletRequest request) {
		log.info("*************the callback request: {}", XMLFormatter.format(sdpRequest));
		String sourceIP = request.getRemoteAddr();
		log.info("*************the remote addres: {}", sourceIP);
		String responseXml = businessLogic.callback(sdpRequest, merchantId, productId);
		return responseXml;
	}

	// for other networks - Vodafone and Tigo
	@PostMapping("/local/callback/{merchantId}/{productId}")
	public CallbackResponse localCallback(@PathVariable String merchantId, @PathVariable String productId,
			@RequestBody CallbackRequest callbackRequest) {
		CallbackResponse callbackResponse = new CallbackResponse();
		ProductTable foundProductRecord = productTableRepository.findByMerchantIdAndProductId(merchantId, productId);
		if (foundProductRecord != null) {
			String url = foundProductRecord.getRequestPaymentCallbackUrl();

			// Call third party back
			log.info("***************Hitting {} for a response****************");
			log.info("***************the request going : {}", callbackRequest);
			callbackResponse = restTemplate.postForObject(url, callbackRequest, CallbackResponse.class);
			log.info("***************the response coming : {}", callbackResponse);

		} else {
			callbackResponse.setResponseCode(ResponseCode.NOT_FOUND);
			callbackResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
		}
		return callbackResponse;
	}
}

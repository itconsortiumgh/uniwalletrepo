package com.itconsortiumgh.uniwallet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.itconsortiumgh.uniwallet.model.CRUDResponse;
import com.itconsortiumgh.uniwallet.model.ResponseCode;
import com.itconsortiumgh.uniwallet.model.ResponseMessage;
import com.itconsortiumgh.uniwallet.model.db.MerchantTable;
import com.itconsortiumgh.uniwallet.repository.MerchantTableRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class MerchantController {
	
	@Autowired
	MerchantTableRepository merchantTableRepository;
	
	@PostMapping("create/merchant")
	public CRUDResponse createMerchant(@RequestBody MerchantTable merchant){
		log.info("************The Json request : {}",merchant);
		CRUDResponse crudResponse = new CRUDResponse();
		try {
			if(merchant != null){
				MerchantTable savedMerchant = merchantTableRepository.save(merchant);
				if(savedMerchant != null){
					crudResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
					crudResponse.setResponseMessage(ResponseMessage.MERCHANT_CREATE_SUCCESS);
					crudResponse.setMerchant(savedMerchant);
				}else{
					crudResponse.setResponseCode(ResponseCode.MERCHANT_CREATE_FAIL);
					crudResponse.setResponseMessage(ResponseMessage.MERCHANT_CREATE_FAIL);
				}
			}else{
				crudResponse.setResponseCode(ResponseCode.INVALID_REQUEST);
				crudResponse.setResponseMessage(ResponseMessage.INVALID_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			crudResponse.setResponseCode(ResponseCode.DATA_VIOLATION);
			crudResponse.setResponseMessage(ResponseMessage.DATA_VIOLATION);
		}
		return crudResponse;
	}
	
	@GetMapping("retrieve/merchant/id/{id}")
	public CRUDResponse retrieveMerchant(@PathVariable String id){
		log.info("************The merchant ID : {}",id);
		CRUDResponse crudResponse = new CRUDResponse();
		MerchantTable foundMerchant = merchantTableRepository.findByMerchantId(id);
		if(foundMerchant != null){
			crudResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
			crudResponse.setResponseMessage(ResponseMessage.MERCHANT_RETRIEVE_SUCCESS);
			crudResponse.setMerchant(foundMerchant);;
		}else{
			crudResponse.setResponseCode(ResponseCode.NOT_FOUND);
			crudResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
		}
		return crudResponse;
	}
	
	@PostMapping("update/merchant")
	public CRUDResponse updateMerchant(@RequestBody MerchantTable merchant){
		log.info("************The Json request : {}",merchant);
		CRUDResponse crudResponse = new CRUDResponse();
		if(merchant != null){
			MerchantTable foundMerchant = merchantTableRepository.findByMerchantId(merchant.getMerchantId());
			if(foundMerchant != null){
				if(merchant.getMerchantName() != null)
					foundMerchant.setMerchantName(merchant.getMerchantName());
//				if(merchant.getMerchantShortName() != null)
//					foundMerchant.setMerchantShortName(merchant.getMerchantShortName());
				if(merchant.getMerchantCategory() != null)
					foundMerchant.setMerchantCategory(merchant.getMerchantCategory());
				if(merchant.getAddress() != null)
					foundMerchant.setAddress(merchant.getAddress());
				if(merchant.getSupportEmail() != null)
					foundMerchant.setSupportEmail(merchant.getSupportEmail());
				if(merchant.getSupportMobile() != null)
					foundMerchant.setSupportMobile(merchant.getSupportMobile());
				if(merchant.getActive() != null)
					foundMerchant.setActive(merchant.getActive());
//				if(merchant.getCreateMandateApiKey() != null)
//					foundMerchant.setCreateMandateApiKey(merchant.getCreateMandateApiKey());
//				if(merchant.getGetPacketApiKey() != null)
//					foundMerchant.setGetPacketApiKey(merchant.getGetPacketApiKey());
//				if(merchant.getOva() != null)
//					foundMerchant.setOva(merchant.getOva());
				MerchantTable savedMerchant = merchantTableRepository.save(foundMerchant);
				if(savedMerchant != null){
					crudResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
					crudResponse.setResponseMessage(ResponseMessage.MERCHANT_UPDATE_SUCCESS);
					crudResponse.setMerchant(savedMerchant);
				}else{
					crudResponse.setResponseCode(ResponseCode.MERCHANT_UPDATE_FAIL);
					crudResponse.setResponseMessage(ResponseMessage.MERCHANT_UPDATE_FAIL);
				}
			}else{
				crudResponse.setResponseCode(ResponseCode.NOT_FOUND);
				crudResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
			}
		}else{
			crudResponse.setResponseCode(ResponseCode.INVALID_REQUEST);
			crudResponse.setResponseMessage(ResponseMessage.INVALID_REQUEST);
		}
		return crudResponse;
	}
	
	@GetMapping("delete/merchant/id/{id}")
	public CRUDResponse deleteMerchant(@PathVariable String id){
		log.info("************The merchant ID : {}",id);
		CRUDResponse crudResponse = new CRUDResponse();
		MerchantTable foundMerchant = merchantTableRepository.findByMerchantId(id);
		if(foundMerchant != null){
			merchantTableRepository.delete(foundMerchant);
			crudResponse.setResponseCode(ResponseCode.GENERAL_SUCCESS);
			crudResponse.setResponseMessage(ResponseMessage.MERCHANT_DELETE_SUCCESS);
			crudResponse.setMerchant(foundMerchant);
		}else{
			crudResponse.setResponseCode(ResponseCode.NOT_FOUND);
			crudResponse.setResponseMessage(ResponseMessage.NOT_FOUND);
		}
		return crudResponse;
	}
}

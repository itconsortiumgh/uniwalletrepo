package com.itconsortiumgh.uniwallet;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.uniwallet.amqp.RabbitMQSubscriber;
import com.itconsortiumgh.uniwallet.properties.ApplicationProperties;

@SpringBootApplication
public class UniWalletApiApplication {
//	@Bean
//	ConnectionFactory uniWalletConnectionFactory(){
//		return new CachingConnectionFactory();
//	}

	public static void main(String[] args) {
		SpringApplication.run(UniWalletApiApplication.class, args);
	}
	
	@Bean
	RestTemplate restTemplate(){
		return new RestTemplate();
	}
	
	@Autowired
	ApplicationProperties applicationProperties;
	
    @Bean
    Queue queue() {
        return new Queue(applicationProperties.getUniQueueName(), false);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(applicationProperties.getUniQueueExchange());
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(applicationProperties.getUniQueueName());
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
            MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(applicationProperties.getUniQueueName());
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(RabbitMQSubscriber rabbitMQSubscriber) {
        return new MessageListenerAdapter(rabbitMQSubscriber, applicationProperties.getReceiveMessage());
    }
}
